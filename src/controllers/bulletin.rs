extern crate iron;
extern crate hyper;
extern crate router;

use iron::prelude::*;
use iron::status;
use models::bulletin;
use std::string::String;

pub fn get_all(_: &mut Request) -> IronResult<Response> {
    let bs = bulletin::get_all();
    let inner = bs.iter()
        .map(|b| b.to_json())
        .collect::<Vec<String>>()
        .join(",");
    let json = format!("[{}]", inner);

    let mut r = Response::with((iron::status::Ok, json));
    r.headers.set_raw("Content-Type", vec![b"application/json".to_vec()]);
    Ok(r)
}

pub fn get(req: &mut Request) -> IronResult<Response> {
    let ref id = req.extensions.get::<router::Router>().unwrap().find("id").unwrap_or("/");

    match bulletin::get(String::from(*id)) {
        None => Ok(Response::with((status::NotFound, "Not found"))),
        Some(b) => {
            let mut resp = Response::with((status::Ok, b.to_json()));
            resp.headers.set_raw("Content-Type", vec![b"application/json".to_vec()]);
            Ok(resp)
        },
    }
}
