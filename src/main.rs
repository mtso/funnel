extern crate iron;
extern crate postgres;
extern crate chrono;

use iron::prelude::*;
use std::env;

mod controllers;
mod models;
mod routes;

// #[macro_use]
// extern crate serde_derive;

use routes::make_router;

fn main() {
    let port = env::var("PORT").unwrap();
    // let connection_string = env::var("DATABASE_URI").unwrap();

    let _server = Iron::new(make_router())
        .http(format!("localhost:{}", port))
        .unwrap();
    println!("Listening on {}", port);
}
