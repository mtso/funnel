extern crate router;

use self::router::Router;

use controllers;

pub fn make_router() -> Router {
    let mut router = Router::new();
    router.get("/bulletin", controllers::bulletin::get_all, "bulletin::get_all");
    router.get("/bulletin/:id", controllers::bulletin::get, "bulletin::get");
    router
}
