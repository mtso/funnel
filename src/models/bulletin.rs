extern crate chrono;

use chrono::{NaiveDate, NaiveTime};
use std::env;
use postgres::{Connection, TlsMode};
use std::string::String;

// extern crate serde;
// extern crate serde_json;

// #[derive(Serialize)]
pub struct Bulletin {
    pub id: i64,
    pub url: String,
    pub archive_url: String,
    pub summary: String,
    pub tag: String,
    pub date: NaiveDate,
}

macro_rules! BULLETIN_JSON_FMT { () => { r#"{{"id":{},"url":"{}","archive_url":"{}","summary":"{}","tag":"{}","date":"{}"}}"# }; }

impl Bulletin {
    pub fn to_json(&self) -> String {
        // Add time back to date and format for json.
        let m = NaiveTime::from_hms_milli(0, 0, 0, 0);
        let tz = format!("{}T{}Z", &self.date, m);
        format!(
            BULLETIN_JSON_FMT!(),
            &self.id,
            &self.url,
            &self.archive_url,
            &self.summary,
            &self.tag,
            tz)
    }
}

pub fn get_all() -> Vec<Bulletin> {
    let conn_string = env::var("DATABASE_URI").unwrap();
    let conn = Connection::connect(conn_string, TlsMode::None).unwrap();
    let all_query = "SELECT b.id, b.url, b.archive_url, b.summary, t.name as tag, b.date
        FROM bulletin b
        INNER JOIN tag t on b.tag_id = t.id";
    
    let mut bulletins: Vec<Bulletin> = Vec::new(); 
    for row in &conn.query(all_query, &[]).unwrap() {
        bulletins.push(Bulletin {
            id: row.get(0),
            url: row.get(1),
            archive_url: row.get(2),
            summary: row.get(3),
            tag: row.get(4),
            date: row.get(5),
        });
    }
    bulletins
}

pub fn get(id: String) -> Option<Bulletin> {
    let conn_string = env::var("DATABASE_URI").unwrap();
    let conn = Connection::connect(conn_string, TlsMode::None).unwrap();
    let select_query = "SELECT b.id, b.url, b.archive_url, b.summary, t.name as tag, b.date
        FROM bulletin b
        INNER JOIN tag t on b.tag_id = t.id
        WHERE b.id = $1";

    let id = id.as_str().parse::<i64>().unwrap();
    let rows = &conn.query(select_query, &[&id]).unwrap();

    match rows.len() {
        0 => None,
        _ => {
            let first = rows.get(0);
            Some(Bulletin {
                id: first.get(0),
                url: first.get(1),
                archive_url: first.get(2),
                summary: first.get(3),
                tag: first.get(4),
                date: first.get(5),
            })
        },
    }
}
